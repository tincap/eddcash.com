<?php $__env->startSection('content'); ?>
    <div class="well no-padding" style="width: 100%;">
        <form method="post" class="form-horizontal"
              style="margin: 20px auto; width: 350px; border-radius: 5px; border: none; background-color: #ffff; color: black; padding: 15px;">

            <?php if(count($errors) > 0): ?>
                <div class="alert alert-danger">
                    <ul style="margin: 10px; background-color: coral; padding: 10px;">
                        <?php foreach($errors->all() as $error): ?>
                            <li style="padding: 5px 0"><?php echo e($error); ?></li>
                        <?php endforeach; ?>
                    </ul>
                </div>
            <?php endif; ?>

            <?php if(session('status')): ?>
                <div class="alert alert-success"
                     style="padding: 10px; background-color: lightgreen; text-align: center;">
                    <?php echo e(session('status')); ?>

                </div>
            <?php endif; ?>

            <?php echo e(csrf_field()); ?>


            <h1 style="color: black">Регистрация пользователя</h1>

            <div class="control-group" style="display: flex; align-items: center; margin: 10px 0;">
                <label class="control-label" for="email" style="width: 150px; font-size: 22px;">E-mail</label>
                <div class="controls">
                    <input style="border: none; border-bottom: 1px solid grey; outline: 0; padding: 5px 10px; font-size: 22px;"
                           type="email" name="email" placeholder="Введите e-mail"
                           class="form-control input-block-level">
                </div>
            </div>

            <div class="control-group" style="display: flex; align-items: center;  margin: 10px 0;">
                <label class="control-label" for="password" style="width: 150px; font-size: 22px;">Пароль</label>
                <div class="controls">
                    <input style="border: none; border-bottom: 1px solid grey;  outline: 0; padding: 5px 10px; font-size: 22px;"
                           type="password" name="password" placeholder="Введите пароль"
                           class="form-control input-block-level">
                </div>
            </div>

            <div class="control-group" style="display: flex; align-items: center;  margin: 10px 0;">
                <label class="control-label" for="password_confirmation" style="width: 150px; font-size: 22px;">Повтор
                    пароля</label>
                <div class="controls">
                    <input style="border: none; border-bottom: 1px solid grey;  outline: 0; padding: 5px 10px; font-size: 22px;"
                           type="password" name="password_confirmation" placeholder="Повторите пароль"
                           class="form-control input-block-level">
                </div>
            </div>

            <div style="display: flex; align-items: center; justify-content: center; margin: 20px 0;">
                <div class="form-actions">
                    <button type="submit" class="btn btn-primary">Зарегистрироваться</button>
                </div>

                <?php /*<div class="form-actions">*/ ?>
                    <?php /*<a href="/sign-in/vk">Войти через <span class="flaticon-soc-vk"></span></a>*/ ?>
                <?php /*</div>*/ ?>
            </div>
        </form>
    </div>
<?php $__env->stopSection(); ?>
<?php echo $__env->make('layout', array_except(get_defined_vars(), array('__data', '__path')))->render(); ?>