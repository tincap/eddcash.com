<?php $__env->startSection('content'); ?>
    <main class="content">
        <div class="inner">
            <div class="account">
                <div class="userbox">
                    <div class="l">
                        <a href="/logout" class="logout"><?php echo app('translator')->get("messages.Logout"); ?></a>
                        <img src="<?php echo e($u->avatar); ?>" alt="<?php echo e($u->username); ?>">
                        <h1><?php echo e($u->username); ?> <a href="https://vk.com/<?php echo e($u->login); ?>" target="_blank"><span
                                        class="flaticon-soc-vk"></span></a></h1>
                        <div class="u-cases"><span class="flaticon-case"></span> <?php echo app('translator')->get("messages.Cases"); ?>: <span class="n"><?php echo e($case); ?></span></div>
                        <div class="u-money"><span class="flaticon-money"></span> <?php echo app('translator')->get("messages.Win"); ?>: <span class="n"><?php echo e($win); ?>р</span>
                        </div>
                        <div class="u-balance"><span class="flaticon-ruble"></span> <?php echo app('translator')->get("messages.Balance"); ?>: <span class="n"><b
                                        id="u-balance-field"><?php echo e($u->money); ?></b>р</span></div>
                    </div>
                    <div class="r">

                        <a href="#deposit" onclick="location.href = '<?php echo e(route('manual.paytm')); ?>'" class="btn yellow"><?php echo app('translator')->get("messages.Fill_in_the_balance"); ?></a>
                        <a href="#withdrawal" class="btn darkblue"
                           onclick="popupOpen('#withdrawal');return false;"><?php echo app('translator')->get("messages.Withdrawal"); ?></a>
                    </div>
                    <div class="cls"></div>
                </div>
                <div class="referral">
                    <div class="b1">
                        <h3><span class="flaticon-users"></span> <?php echo app('translator')->get("messages.Invite_your_friends_and_earn_more"); ?></h3>
                        <div class="desc"><?php echo app('translator')->get("messages.Send_your_unique_code_to_your_frieds"); ?><br> <?php echo app('translator')->get("messages.and"); ?> <span><?php echo app('translator')->get("messages.get_5_%"); ?></span> <?php echo app('translator')->get("messages.from_each_friend's_filling_in"); ?>
                        </div>
                        <div class="field"><input type="text" class="inp" value="<?php echo e($u->ref_code); ?>" readonly="readonly"
                                                  onclick="select();"></div>
                        <div class="short"><?php echo app('translator')->get("messages.Your_code_was_used_to_register"); ?>: <?php echo e($count_ref); ?></div>
                    </div>
                    <div class="b2">
                        <div class="loader" id="redeem-loader"><img src="templates/frontend/default/images/loader.svg"
                                                                    alt=""></div>
                        <?php if($u->ref_use == NULL): ?>
                            <h3><span class="flaticon-money"></span> <?php echo app('translator')->get("messages.Enter_a_code_and_get_10_r_!"); ?></h3>
                            <div class="desc"><?php echo app('translator')->get("messages.Do_you_have_an_affiliate_code_?"); ?><br> <?php echo app('translator')->get("messages.Enter_it_in_a_field_and"); ?>
                                <span><?php echo app('translator')->get("messages.Get_10_rubles"); ?></span> <?php echo app('translator')->get("messages.Right_now!"); ?>
                            </div>
                            <div class="field">
                                <input type="text" class="inp redeem-input">
                                <input type="button" class="btn" value="OK"
                                       onclick="RedeemCode($('.redeem-input'), this, '#redeem-loader');">
                            </div>
                            <div class="short"><?php echo app('translator')->get("messages.Enter_a_code_and_push_Enter"); ?></div>
                        <?php else: ?>
                            <h3><span class="flaticon-money"></span> <?php echo app('translator')->get("messages.Enter_a_code_and_get_10_r_!"); ?></h3>
                            <div class="desc"><?php echo app('translator')->get("messages.Do_you_have_an_affiliate_code_?"); ?><br> <?php echo app('translator')->get("messages.Enter_it_in_the_field_and"); ?>
                                <span><?php echo app('translator')->get("messages.get_10_rubles"); ?></span> <?php echo app('translator')->get("messages.Right_now_!"); ?>
                            </div>
                            <div class="field">
                                <input type="text" class="inp redeem-input" value="<?php echo e($u->ref_use); ?>">
                            </div>
                            <div class="short"><span class="flaticon-check"></span><?php echo app('translator')->get("messages.The_code_is_successfully_activated!"); ?></div>
                        <?php endif; ?>
                    </div>
                    <div class="cls"></div>
                </div>
                <div class="tabs">
                    <div class="tab tab-1 eas active" data-tab-id="1"><?php echo app('translator')->get("messages.Prizes!"); ?></div>
                    <div class="tab tab-2 eas" data-tab-id="2"><?php echo app('translator')->get("messages.History"); ?></div>
                    <div class="tab tab-3 eas" data-tab-id="3"><?php echo app('translator')->get("messages.Finance"); ?></div>
                    <div class="tab tab-4 eas" data-tab-id="4"><?php echo app('translator')->get("messages.Delivery"); ?></div>
                </div>
                <div class="tab-container tab-container-1 active">
                    <div class="cls"></div>
                    <?php if($items == []): ?>
                        <div class="infobox text-center"><br>
                            <h3>
                                <center><?php echo app('translator')->get("messages.You_didn't_open_cases"); ?> :(</center>
                            </h3>
                            <a href="/" class="btn rounded blue"><?php echo app('translator')->get("messages.Open_and_win"); ?></a><br><br>
                        </div>
                    <?php else: ?>
                        <div class="history-cases">
                            <div class="cls"></div>
                            <?php foreach($gifts as $gift): ?>
                                <div class="history-case">
                                    <a href="/cases/<?php echo e($gift->case_id); ?>" class="eas case-url"><?php echo app('translator')->get("messages.Case_№"); ?><?php echo e($gift->case_id); ?></a>
                                    <?php if($gift->status == 0): ?>
                                        <div class="status3" id="send" title="Send item" data-id="<?php echo e($gift->id); ?>">
                                            ⥯
                                        </div>
                                        <div class="status2" id="sell" title="Sell gift for 50%"   data-id="<?php echo e($gift->id); ?>">
                                            $
                                        </div>
                                    <?php endif; ?>
                                    <?php if($gift->status == 1): ?>
                                        <div class="status">
                                            $
                                        </div>
                                    <?php endif; ?>
                                    <?php if($gift->status == 2): ?>
                                        <div class="status">
                                            ♺
                                        </div>
                                    <?php endif; ?>
                                    <?php if($gift->status == 3): ?>
                                        <div class="status">
                                            <span class="flaticon-check"></span>
                                        </div>
                                    <?php endif; ?>
                                    <div class="coin silver">
                                        <img src="<?php echo e($gift->img); ?>" alt="<?php echo e($gift->price); ?> рублей">
                                    </div>
                                </div>
                            <?php endforeach; ?>
                            <?php foreach($items as $i): ?>
                                <?php if($i->price == 12345): ?>

                                <?php else: ?>
                                    <div class="history-case">
                                        <a href="/cases/<?php echo e($i->cases_id); ?>" class="eas case-url"><?php echo app('translator')->get("messages.Case_№"); ?><?php echo e($i->cases_id); ?></a>
                                        <div class="status">
                                            <span class="flaticon-check"></span>
                                        </div>
                                        <div class="coin silver">
                                            <img src="<?php echo e($i->img); ?>" alt="<?php echo e($i->price); ?> рублей">
                                        </div>
                                    </div>
                                <?php endif; ?>
                            <?php endforeach; ?>
                            <div class="cls"></div>
                        </div>
                    <?php endif; ?>
                </div>
                <div class="tab-container tab-container-2">
                    <div class="part part-header">
                        <div class="p-n"><?php echo app('translator')->get("messages.Case"); ?></div>
                        <div class="p-id"><?php echo app('translator')->get("messages.Spent"); ?></div>
                        <div class="p-name"><?php echo app('translator')->get("messages.Got"); ?></div>
                        <div class="cls"></div>
                    </div>
                    <?php if($cases == null): ?>
                        <div class="infobox">
                            <div class='text-center'><?php echo app('translator')->get("messages.Soon_your_billions_will_be_here"); ?>
                            </div>
                        </div>
                    <?php else: ?>
                        <?php foreach($cases as $case): ?>
                            <div class="part">
                                <div class="p-n"><a href="/cases/<?php echo e($case->case_id); ?>">#<?php echo e($case->case_id); ?></a></div>
                                <div class="p-id"><?php echo e($case->case_price); ?></div>
                                <div class="p-name">
                                    <?php if($case->price == 12345): ?>
                                        <?php echo e($case->name); ?>

                                    <?php else: ?>
                                        <?php echo e($case->price); ?>

                                    <?php endif; ?></div>
                                <div class="cls"></div>
                            </div>
                        <?php endforeach; ?>
                    <?php endif; ?>
                </div>
                <div class="tab-container tab-container-3">
                    <div class="part part-header">
                        <div class="p-n">№</div>
                        <div class="p-id"><?php echo app('translator')->get("messages.Sum"); ?></div>
                        <div class="p-name"><?php echo app('translator')->get("messages.System"); ?></div>
                        <div class="p-games"><?php echo app('translator')->get("messages.Action"); ?></div>
                        <div class="cls"></div>
                    </div>
                    <?php if($mo == null && $mo2 == null && $vivod == null): ?>
                        <div class="infobox">
                            <div class='text-center'><?php echo app('translator')->get("messages.Soon_your_billions_will_be_here"); ?></div>
                        </div>
                    <?php else: ?>
                        <?php foreach($vivod as $o): ?>
                            <div class="part">
                                <div class="p-n">#<?php echo e($o->id); ?></div>
                                <div class="p-id"><?php echo e($o->price); ?></div>
                                <div class="p-name"><?php echo e($o->koshel); ?></div>
                                <div class="p-games">
                                    <?php if($o->status == 0): ?>
                                        <?php echo app('translator')->get("messages.Withdrawal"); ?> (Ожидает)
                                    <?php else: ?>
                                        <?php echo app('translator')->get("messages.Withdrawal"); ?> (Переведен)
                                    <?php endif; ?>
                                </div>
                                <div class="cls"></div>
                            </div>
                        <?php endforeach; ?>
                        <?php foreach($mo as $co): ?>
                            <div class="part">
                                <div class="p-n">#<?php echo e($co->id); ?></div>
                                <div class="p-id"><?php echo e($co->amount); ?></div>
                                <div class="p-name">Free-kassa</div>
                                <div class="p-games"><?php echo app('translator')->get("messages.Filling_in"); ?></div>
                                <div class="cls"></div>
                            </div>
                        <?php endforeach; ?>
                        <?php foreach($mo2 as $co2): ?>
                            <div class="part">
                                <div class="p-n">#<?php echo e($co2->id); ?></div>
                                <div class="p-id"><?php echo e($co2->price); ?></div>
                                <div class="p-name">Interkassa</div>
                                <div class="p-games"><?php echo app('translator')->get("messages.Filling_in"); ?></div>
                                <div class="cls"></div>
                            </div>
                        <?php endforeach; ?>
                    <?php endif; ?>

                </div>
                <div class="tab-container tab-container-4"><div class="loader" id="delivery-loader"><img src="templates/frontend/default/images/loader.svg" alt=""></div>
                    <table class="table history-money" id="shipping-table-head">
                        <thead>
                        <tr>
                            <th width="50">№</th>
                            <th width="200"><?php echo app('translator')->get("messages.Gift"); ?></th>
                            <th width="150" class="text-center"><?php echo app('translator')->get("messages.Date"); ?></th>
                            <th width="80" class="text-center"><?php echo app('translator')->get("messages.Status"); ?></th>
                        </tr>
                        </thead>
                        <?php foreach($gifts as $gift): ?>
                            <tr>
                                <td width="50"><?php echo e($gift->id); ?></td>
                                <td width="200"><?php echo e($gift->name); ?></td>
                                <td width="150" class="text-center"><?php echo e($gift->updated_at); ?></td>
                                <td width="80" class="text-center">
                                    <?php if($gift->status == 0): ?>
                                        <?php echo app('translator')->get("messages.Waiting"); ?>
                                    <?php endif; ?>

                                    <?php if($gift->status == 1): ?>
                                        <?php echo app('translator')->get("messages.Sold"); ?>
                                    <?php endif; ?>
                                    <?php if($gift->status == 2): ?>
                                        <?php echo app('translator')->get("messages.Processing"); ?>
                                    <?php endif; ?>
                                    <?php if($gift->status == 3): ?>
                                        <?php echo app('translator')->get("messages.Received"); ?>
                                    <?php endif; ?>
                                </td>
                            </tr>
                        <?php endforeach; ?>
                    </table>
                    <div id="history-shipping">
                    </div>
                </div>
            </div>
        </div>
        <div class="seperator"></div>
        </div>
    </main>
    <script>
        $(document).ready(function () {
            $('#send').click(function() {
                var id = $('#send').attr("data-id");
                var user2 = '<?php echo e($u->id); ?>';
                $.ajax({
                    url: '/api/send',
                    type: 'post',
                    data: {item: id, user: user2},
                    dataType: 'json',
                    success: function(rdata){
                        if(rdata.success == true){
                            setTimeout(location.reload(), 2000);
                        }else{
                            smoke.alert(rdata.error);
                        }
                    }
                });
            });
            $('#sell').click(function() {
                var id = $('#sell').attr("data-id");
                var user2 = '<?php echo e($u->id); ?>';
                $.ajax({
                    url: '/api/sell',
                    type: 'post',
                    data: {item: id, user: user2},
                    dataType: 'json',
                    success: function(rdata){
                        if(rdata.success == true){
                            setTimeout(location.reload(), 2000);
                        }else{
                            smoke.alert(rdata.error);
                        }
                    }
                });
            });
        });

    </script>
<?php $__env->stopSection(); ?>

<?php echo $__env->make('users', array_except(get_defined_vars(), array('__data', '__path')))->render(); ?>