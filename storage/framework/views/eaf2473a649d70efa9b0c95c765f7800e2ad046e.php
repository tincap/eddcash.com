﻿

<?php $__env->startSection('content'); ?>
    <main class="content">
        <div class="inner"><h1 class="title"><?php echo app('translator')->get("messages.guarantees"); ?></h1>
            <div class="cls"></div>
            <div class="static">
                <div class="row">
                    <div class="static-content">
                        <div class="b3 b3-1">
                            <h3><img src="uploads/images/faq-b1.png" alt="Безопасно"/> <?php echo app('translator')->get("messages.safe"); ?></h3>
                            <?php echo app('translator')->get("messages.We_use_a_certificate_of_256_bit,_that_protects_your_transactions_and_authorizations_on_the_website._You_will_be_safe_with_us_!"); ?>
                        </div>
                        <div class="b3 b3-2">
                            <h3><img src="uploads/images/faq-b2.png" alt="Честно"/> <?php echo app('translator')->get("messages.honestly"); ?></h3>
                            <?php echo app('translator')->get("messages.Playsy_system_is_based_on_ticket_algorithm_that_determines_the_number_taking_into_consideration_a_coefficient_and_random_number_generation"); ?>
                        </div>
                        <div class="b3 b3-3">
                            <h3><img src="uploads/images/faq-b3.png" alt="Моментально"/> <?php echo app('translator')->get("messages.Immediately"); ?></h3>
                            <?php echo app('translator')->get("messages.You_will_get_your_win_on_the_system_balance_immediately._Also_you_can_exchange_a_digital_item_to_the_money_equivalent - it's_immediately_too"); ?>
                        </div>
                        <div class="cls">&nbsp;</div>
                        <div class="h-title">
                            <h1><?php echo app('translator')->get("messages.What_do_we_have_?"); ?></h1>
                        </div>
                        <div class="cls">&nbsp;</div>
                        <div class="guarantee-list">
                            <div class="l">1</div>
                            <div class="r">
                                <h4><?php echo app('translator')->get("messages.Personal_webmoney_passport"); ?></h4>
                                <?php echo app('translator')->get("messages.Personal_passport_is_given_to_a_participant_of_Webmoney_Transfer_system_after_checking_his_personal_(passport)_data_by_one_of_the_registrators-participants_of_the_affiliate_program_of_the_Verification_center"); ?>
                            </div>
                        </div>
                        <div class="guarantee-list">
                            <div class="l">2</div>
                            <div class="r">
                                <h4><?php echo app('translator')->get("messages.Identified_yandex_wallet"); ?></h4>
                                <?php echo app('translator')->get("messages.The_procedure_of_identification_in_Yandex_was_provided_by_us._Our_personal_data,_including_passport,_address_of_registration_was_notirized_and_given_to_Yandex_officers"); ?>
                            </div>
                        </div>
                        <div class="guarantee-list">
                            <div class="l">3</div>
                            <div class="r">
                                <h4><?php echo app('translator')->get("messages.Contract_with_processing_services"); ?></h4>
                                <?php echo app('translator')->get("messages.Contracts_were_signed_and_delivered_on_making_Internet_transactions_to_personal_Playsy_accounts._This_procedure_is_provided_by_many_processing_services_and_was_passed_by_us"); ?>
                            </div>
                        </div>
                        <div class="cls">&nbsp;</div>
                    </div>
                    <div class="cls">&nbsp;</div>
                    <h3 class="h3left"><?php echo app('translator')->get("messages.Any_doubts_?"); ?></h3>
                    <div class="guarantee-note">
                        <div class="l"><span class="flaticon-cry">&nbsp;</span></div>
                        <div class="r"><?php echo app('translator')->get("messages.Dear_friend,_we_don't_make_anyone_participate_in_this_project._You_do_it_because_of_your_desire_and_will._We_need_the_fact_of_project_raising_as_we_earn_with_it._If_not_to_be_honest-the_project_will_not_exist._If_you_have_any_doubts,_go_to_the_home_page_,and_see_all_the_wins_of_Live._Feel_free_to_contact_with_any_participant_and_ask_about_anything_(I_hope_they_will_answer)"); ?>
                            <br/><br/><?php echo app('translator')->get("messages.If_you_have_any_questions_or_something_is_unclear,_feel_free"); ?> <a
                                    href="support/"><?php echo app('translator')->get("messages.write_to_us"); ?></a>, <?php echo app('translator')->get("messages.we_will_be_glad_to_answer_!"); ?>
                        </div>
                    </div>
                    <div class="cls">&nbsp;</div>
                </div>
            </div>
            <div class="cls"></div>
        </div>
    </main>
<?php $__env->stopSection(); ?>
<?php echo $__env->make('layout', array_except(get_defined_vars(), array('__data', '__path')))->render(); ?>