<?php $__env->startSection('content'); ?>
    <main class="content">
        <div class="inner"><h1 class="title">FAQ</h1>
            <div class="cls"></div><div class="static"><div class="cls">&nbsp;</div>
                <div class="b3 b3-1">
                    <h3><img src="uploads/faq-b1.png" alt="Безопасно" /> <?php echo app('translator')->get("messages.Безопасно"); ?></h3>
                    <?php echo app('translator')->get("messages.Мы используем 256 битный сертификат, который защищает ваши транзакции и авторизации на сайте. С нами вы будете в безопасности"); ?>!</div>
                <div class="b3 b3-2">
                    <h3><img src="uploads/faq-b2.png" alt="Честно" /> <?php echo app('translator')->get("messages.Честно"); ?></h3>
                    <?php echo app('translator')->get("messages.Система Epicdrop основана на алгоритме, который определяет число с учетом коэффицента и случайной генерации числа 0-100"); ?>.</div>
                <div class="b3 b3-3">
                    <h3><img src="uploads/faq-b3.png" alt="Моментально" /> <?php echo app('translator')->get("messages.Моментально"); ?></h3>
                    <?php echo app('translator')->get("messages.Вы получите свой выигрыш моментально на баланс системы"); ?>.</div>
                <div class="cls">&nbsp;</div>
                <div class="faq">
                    <div class="question"><?php echo app('translator')->get("messages.Как можно пополнить баланс"); ?>?</div>
                    Баланс на нашем сайте вы можете пополнить абсолютно разными способами: Яндекс.Деньги, Qiwi и т.п<br /> Чтобы пополнить баланс, перейдите в свой личный кабинет и нажмите "Пополнить баланс"</div>
                <div class="faq">
                    <div class="question"><?php echo app('translator')->get("messages.Как вывести средства"); ?>?</div>
                    Средства вы сможете вывести зайдя в свой профиль, нажав "Вывод средств"</div>
                <div class="faq">
                    <div class="question"><?php echo app('translator')->get("messages.Как определяется приз"); ?>?</div>
                    <?php echo app('translator')->get("messages.Программа для вычисления победителя основана на генерации случайного числа исходя из процентного шанса приза. Чем дороже приз - тем меньше шанс выпадения"); ?>.</div>
                <div class="faq">
                    <div class="question"><?php echo app('translator')->get("messages.Что такое партнерский код"); ?>?</div>
                    <?php echo app('translator')->get("messages.Партнерский код - это код, благодаря которому Вы сможете дополнительно зарабатывать на нашем проекте. Делитесь кодом с Вашими друзьями и получайте 5% от каждой транзакции"); ?>.</div>
                <div class="faq">
                    <div class="question"><?php echo app('translator')->get("messages.Куда вводить партнерский код"); ?>?</div>
                    <?php echo app('translator')->get("messages.Вводить партнерский код нужно в своем личном кабинете. За это вы получаете бонус в размере 10 рублей"); ?>.</div>
                <div class="faq">
                    <div class="question"><?php echo app('translator')->get("messages.Что такое увеличитель шанса"); ?>?</div>
                    <?php echo app('translator')->get("messages.Увеличитель шанса, дает вам шанс выиграть более дорогой приз чаще чем обычно. Чем выше ваш дополнительный шанс, тем больше вероятности, что вам выпадет более дорогой приз за маленькую плату"); ?>.</div>
                <div class="faq">
                    <div class="question"><?php echo app('translator')->get("messages.Я сделал запрос на вывод средств, но деньги не пришли"); ?>.</div>
                    <?php echo app('translator')->get("messages.Вывод средств производится в течении 24-х часов. Если, вдруг, средства не пришли в течении указанного времени - обратитесь в тех.поддержку"); ?>.</div>
                <div class="touch-now text-center">
                    <h3><?php echo app('translator')->get("messages.Не нашли ответ на ваш вопрос"); ?>?</h3>
                    <a class="btn yellow rounded" href="https://vk.com/im?sel=-128441098"><?php echo app('translator')->get("messages.Задать вопрос"); ?></a></div></div>
            <div class="cls"></div></div>
    </main>
<?php $__env->stopSection(); ?>

<?php echo $__env->make('layout', array_except(get_defined_vars(), array('__data', '__path')))->render(); ?>