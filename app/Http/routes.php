<?php

Route::get('/', ['as' => 'index', 'uses' => 'PagesController@index']);
//Route::get('/sign-in/vk', 'LoginController@vklogin');
Route::match(['get', 'post'],'/login', 'LoginController@index');
Route::match(['get', 'post'], '/register', 'RegisterController@index');
Route::get('/cases/{id}', 'PagesController@cases');
Route::get('/gifts/{id}', 'PagesController@gifts');
Route::get('/ticket/{id}', 'BonusController@ticket');
Route::get('/profile/{id}', 'PagesController@profile');
Route::get('/faq', 'PagesController@faq');
Route::get('/guarantee', 'PagesController@guarantee');
Route::get('/reviews', 'PagesController@reviews');
Route::get('/terms', 'PagesController@terms');
Route::post('/payment/accept', 'PagesController@acceptkassa');
Route::get('/success', 'PagesController@success');
Route::get('/contests', 'PagesController@konkurs');
Route::get('/konkurs', 'PagesController@konkurs');
Route::post('/getContestants', 'BonusController@getContestants');

Route::get('/bonus', 'BonusController@bonus');
Route::get('/contest', 'BonusController@contests');
Route::post('/pay', 'PagesController@pay');
Route::post('/getPayment', 'PagesController@getPayment');
Route::post('/join', 'BonusController@hour_join');
Route::post('/hour', 'BonusController@hour');
Route::get('/api/addbonus', ['as' => 'inventory', 'uses' => 'BonusController@addbonus']);
Route::post('/opencase/{id}/{chance}', 'PagesController@opencase');
Route::post('/api/setplace', 'BonusController@setplace');
Route::group(['middleware' => 'auth'], function () {
    Route::get('/logout', 'LoginController@logout');
    Route::get('/account', 'PagesController@account');
    Route::get('/support', 'PagesController@support');
    Route::post('/refuse', 'PagesController@refuse');
    Route::post('/vivod/{price}/{koshelek}', 'PagesController@vivod');
});

Route::group(['prefix' => 'payment'], function () {
    Route::post('/hook', 'PaymentController@hook');
    Route::post('/pay', 'PaymentController@pay')->name('payment.pay');
    Route::get('/fail', 'PaymentController@fail')->name('payment.fail');
    Route::get('/success', 'PaymentController@success')->name('payment.success');
});

Route::group(['prefix' => 'manual'], function () {
    Route::get('/pay/paytm', 'ManualController@paytm')->name('manual.paytm');
});

Route::group(['middleware' => 'admin', 'middleware' => 'access:admin', 'prefix' => 'admin'], function () {
    Route::get('/', 'AdminController@index');
    Route::get('/addCase', 'AdminController@addCase');
    Route::post('/addCase', 'AdminController@addCasePost');
    Route::get('/addItem', 'AdminController@addItem');
    Route::post('/addItem', 'AdminController@addItemPost');
    Route::get('/lastvvod', 'AdminController@lastvvod');
    Route::get('/lastvivod', 'AdminController@vivod');
    Route::get('/vivodgifts', 'AdminController@vivodgifts');
    Route::get('/users', 'AdminController@users');
    Route::get('/cases', 'AdminController@cases');
    Route::get('/tickets', 'AdminController@tickets');
    Route::get('/cases/{id}', ['as' => 'cases', 'uses' => 'AdminController@caseid']);
    Route::get('/ticket/{id}', ['as' => 'ticket', 'uses' => 'AdminController@ticket']);
    Route::post('/ticketsave', ['as' => 'ticket', 'uses' => 'AdminController@ticketsave']);
    Route::post('/casedit', ['as' => 'case', 'uses' => 'AdminController@casedit']);
    Route::get('/searchusers', ['as' => 'search', 'uses' => 'AdminController@search2']);
    Route::get('/searchusersname', ['as' => 'search', 'uses' => 'AdminController@searchusersname']);
    Route::get('/user/{id}', ['as' => 'users', 'uses' => 'AdminController@userid']);
    Route::post('/userdit', ['as' => 'user', 'uses' => 'AdminController@userdit']);
  Route::match(['get', 'post'], '/givemoney/{id}', ['as' => 'givemoney', 'uses' => 'AdminController@givemoney']);
    Route::get('/vivodclose/{id}', 'AdminController@close');
    Route::get('/vivodclosegift/{id}', 'AdminController@closegift');
});


Route::group(['prefix' => 'api'], function () {
    Route::post('/stats', 'PagesController@stats');
    Route::post('/last_drop', 'PagesController@last_drop');
    Route::post('/last_drop_get', 'PagesController@last_gift_get');
    Route::post('/sell', 'PagesController@sell');
    Route::post('/send', 'PagesController@send');
});