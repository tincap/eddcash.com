<?php

namespace App\Http\Controllers;

use App\Payments;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Log;
use Illuminate\Support\Facades\Validator;

class PaymentController extends Controller
{
    private $key = "513844344e6a323231563779423666337c67425e69554c30464160";
    private $merchantId = 112075776068;
    private $currencyId = 643;

    const RESULT_RETRY = 'RETRY';
    const RESULT_OK = 'OK';

    private function answer($result, $description)
    {
        Log::info(print_r([
            'WMI_RESULT' => strtoupper($result),
            'WMI_DESCRIPTION' => urlencode($description)
        ], true));

        print "WMI_RESULT=" . strtoupper($result) . "&";
        print "WMI_DESCRIPTION=" . urlencode($description);
        exit();
    }

    public function hook(Request $request) {
        $validator = Validator::make($request->all(), [
            'WMI_ORDER_STATE' => 'required',
            'WMI_SIGNATURE' => 'required',
            'WMI_PAYMENT_NO' => 'required'
        ]);

        Log::info(print_r($request->all(), true));

        if ($validator->fails()) {
            $this->answer(self::RESULT_RETRY, 'Отсутствуют параметры');
        }

        $post = $request->all();
        $params = [];

        // Извлечение всех параметров POST-запроса, кроме WMI_SIGNATURE

        foreach ($post as $name => $value) {
            if ($name !== 'WMI_SIGNATURE') $params[$name] = $value;
        }

        // Сортировка массива по именам ключей в порядке возрастания
        // и формирование сообщения, путем объединения значений формы

        uksort($params, 'strcasecmp'); $values = "";

        foreach($params as $name => $value)
        {
            $values .= $value;
        }

        // Формирование подписи для сравнения ее с параметром WMI_SIGNATURE

        $signature = base64_encode(pack('H*', sha1($values . $this->key)));

        //Сравнение полученной подписи с подписью W1

        if ($signature == $request->get('WMI_SIGNATURE'))
        {
            if (strtoupper($request->get('WMI_ORDER_STATE')) == 'ACCEPTED')
            {
                preg_match('/-00(.*)/', $request->get('WMI_PAYMENT_NO'), $match);

                if (isset($match[1])) {
                    try {
                        DB::table('payments')
                            ->where('id', $match[1])
                            ->update(['status' => true]);

                        $payment = DB::table('payments')
                            ->where('id', $match[1])
                            ->first();

                        $user = DB::table('users')
                            ->where('id', $payment->user)
                            ->first();

                        DB::table('users')
                            ->where('id', $user->id)
                            ->update(['money' => $user->money + intval($request->get('WMI_PAYMENT_AMOUNT'))]);

                        $this->answer(self::RESULT_OK, "Заказ #" . $_POST["WMI_PAYMENT_NO"] . " оплачен!");
                    } catch (\Exception $e) {
                        Log::info($e->getTraceAsString());
                        $this->answer(self::RESULT_RETRY, "Заказ #" . $_POST["WMI_PAYMENT_NO"] . " не подтвержден!");
                    }
                } else {
                    $this->answer(self::RESULT_RETRY, "Заказ #" . $_POST["WMI_PAYMENT_NO"] . " не подтвержден!");
                }
            }
            else
            {
                // Случилось что-то странное, пришло неизвестное состояние заказа

                $this->answer(self::RESULT_RETRY, "Неверное состояние ". $_POST["WMI_ORDER_STATE"]);
            }
        }
        else
        {
            // Подпись не совпадает, возможно вы поменяли настройки интернет-магазина

            $this->answer(self::RESULT_RETRY, "Неверная подпись " . $_POST["WMI_SIGNATURE"]);
        }
    }

    public function success(Request $request) {
        return view('payment.success');
    }

    public function fail(Request $request) {
        return view('payment.fail');
    }

    public function pay(Request $request) {
        $validator = Validator::make($request->all(), [
            'amount' => 'required|numeric',
        ]);

        if ($validator->fails()) {
            return redirect()->back();
        }

        $user = $request->user();

        if (!$user) {
            return redirect()->back();
        }

        $paymentId = DB::table('payments')->insertGetId([
            'amount' => $request->get('amount'),
            'user' => $user->id,
            'time' => time(),
            'status' => false,
        ]);

        $fields = [];

        $date = date('Ymd');
        $payment = $date . '-00' . $paymentId;

        // Добавление полей формы в ассоциативный массив
        $fields["WMI_MERCHANT_ID"]    = $this->merchantId;
        $fields["WMI_PAYMENT_AMOUNT"] = number_format($request->get('amount'), 2, '.', '');
        $fields["WMI_CURRENCY_ID"]    = $this->currencyId;
        $fields["WMI_PAYMENT_NO"]     = $payment;
        $fields["WMI_DESCRIPTION"]    = "BASE64:".base64_encode("Оплата заказа #$payment на eddcash.ru");
        $fields["WMI_EXPIRED_DATE"]   = gmdate('Y-m-d\TH:i:s', strtotime('+1 day')); // время оплаты сутки
        $fields["WMI_SUCCESS_URL"]    = route('payment.success');
        $fields["WMI_FAIL_URL"]       = route('payment.fail');
        $fields["WMI_CULTURE_ID"]     = "ru-RU";
        //Если требуется задать только определенные способы оплаты, раскоментируйте данную строку и перечислите требуемые способы оплаты.
        //$fields["WMI_PTENABLED"]      = array("UnistreamRUB", "SberbankRUB", "RussianPostRUB");

        //Сортировка значений внутри полей
        foreach($fields as $name => $val)
        {
            if(is_array($val))
            {
                usort($val, "strcasecmp");
                $fields[$name] = $val;
            }
        }

        // Формирование сообщения, путем объединения значений формы,
        // отсортированных по именам ключей в порядке возрастания.
        uksort($fields, "strcasecmp");
        $fieldValues = "";

        foreach($fields as $value)
        {
            if(is_array($value))
                foreach($value as $v)
                {
                    //Конвертация из текущей кодировки (UTF-8)
                    //необходима только если кодировка магазина отлична от Windows-1251
                    $v = iconv("utf-8", "windows-1251", $v);
                    $fieldValues .= $v;
                }
            else
            {
                //Конвертация из текущей кодировки (UTF-8)
                //необходима только если кодировка магазина отлична от Windows-1251
                $value = iconv("utf-8", "windows-1251", $value);
                $fieldValues .= $value;
            }
        }

        // Формирование значения параметра WMI_SIGNATURE, путем
        // вычисления отпечатка, сформированного выше сообщения,
        // по алгоритму MD5 и представление его в Base64

        $signature = base64_encode(pack("H*", sha1($fieldValues . $this->key)));

        //Добавление параметра WMI_SIGNATURE в словарь параметров формы

        $fields["WMI_SIGNATURE"] = $signature;

        return view('payment.pay', [
            'fields' => $fields
        ]);
    }
}