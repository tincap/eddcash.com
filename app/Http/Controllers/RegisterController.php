<?php

namespace App\Http\Controllers;

use App\User;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\Validator;

class RegisterController extends Controller
{
    /**
     * Стандартная регистрация
     * @param Request $request
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\Http\RedirectResponse|\Illuminate\Routing\Redirector|\Illuminate\View\View
     */
    public function index(Request $request) {
        if ($request->isMethod('post')) {
            $validator = Validator::make($request->all(), [
                'email' => 'required|email|unique:users|max:255',
                'password' => 'required|confirmed|min:6'
            ], [
                'email.email' => 'Введите правильный e-mail',
                'email.required' => 'Введите свой e-mail',
                'email.unique' => 'Данный e-mail уже зарегистрирован',
                'email.max' => 'E-mail должен быть меньше 255 символов',
                'password.required' => 'Введите пароль',
                'password.confirmed' => 'Пароли не совпадают',
                'password.min' => 'Пароль должен быть больше 6 символов'
            ]);

            if ($validator->fails()) {
                return redirect('register')
                    ->withErrors($validator);
            }

            User::create([
                'username' => $request->get('email'),
                'email' => $request->get('email'),
                'password' => Hash::make($request->get('password')),
            ]);

            return redirect('register')->with('status', 'Пользователь зарегистрирован!');
        }

        return view('register.index');
    }
}