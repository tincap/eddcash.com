﻿@extends('layout')

@section('content')
    <main class="content">
        <div class="inner"><h1 class="title">@lang("messages.Гарантии")</h1>
            <div class="cls"></div><div class="static"><div class="row">
                    <div class="static-content">
                        <div class="b3 b3-1">
                            <h3><img src="uploads/images/faq-b1.png" alt="Безопасно" /> @lang("messages.Безопасно")</h3>
                            @lang("messages.Мы используем 256 битный сертификат, который защищает ваши транзакции и авторизации на сайте. С нами вы будете в безопасности")!</div>
                        <div class="b3 b3-2">
                            <h3><img src="uploads/images/faq-b2.png" alt="Честно" /> @lang("messages.Честно")</h3>
                            @lang("messages.Система Плейси основана на билетном алгоритме, который определяет число с учетом коэффицента и случайной генерации числа").</div>
                        <div class="b3 b3-3">
                            <h3><img src="uploads/images/faq-b3.png" alt="Моментально" /> @lang("messages.Моментально")</h3>
                            @lang("messages.Вы получите свой выигрыш моментально на баланс системы. Также вы можете обменять цифровой товар на эквивалент в деньгах - тоже моментально")!</div>
                        <div class="cls">&nbsp;</div>
                        <div class="h-title">
                            <h1>@lang("messages.Что мы имеем")?</h1>
                        </div>
                        <div class="cls">&nbsp;</div>
                        <div class="guarantee-list">
                            <div class="l">1</div>
                            <div class="r">
                                <h4>@lang("messages.Персональный аттестат WebMoney")</h4>
                                @lang("messages.Персональный аттестат выдается участнику системы WebMoney Transfer после проверки его персональных (паспортных) данных одним из Регистраторов - участников партнерской программы Центра аттестации").</div>
                        </div>
                        <div class="guarantee-list">
                            <div class="l">2</div>
                            <div class="r">
                                <h4>@lang("messages.Идентифицированный Яндекс кошелек")</h4>
                                @lang("messages.Нами была проведена процедура идентификации в Яндексе. Наши персональные данные, включая паспорт, адрес прописки были нотариально завершены и переданы сотрудникам Яндекса").</div>
                        </div>
                        <div class="guarantee-list">
                            <div class="l">3</div>
                            <div class="r">
                                <h4>@lang("messages.Договоры с процессинговыми сервисами")</h4>
                                @lang("messages.Были подписаны и доставлены договоры, по проведению интернет транзакций на персонализированные счета Плейси. Данная процедура предусматривается многими процессинговыми сервисами и были пройдены нами").</div>
                        </div>
                        <div class="cls">&nbsp;</div>
                    </div>
                    <div class="cls">&nbsp;</div>
                    <h3 class="h3left">@lang("messages.Остались сомнения")?</h3>
                    <div class="guarantee-note">
                        <div class="l"><span class="flaticon-cry">&nbsp;</span></div>
                        <div class="r">Уважаемый друг, мы никого не заставляем принять участие в проекте. Все вы делаете по доброй воле и желанию. Нам важен сам факт поднятия проекта, так как и мы зарабатываем на этом. Не будем честными, не будем порядочными - не бывать проекту. Если уж вы очень сомнительный человек, перейдите на главную, проследите в Live ленте призов последние выигрыши. Вы свободно можете связаться с любым участником проекта (надеюсь будут добрые и ответят) и спросить лично, что к чему.<br /><br />Если у вас остались какие-то вопросы или недопонимания, свободно <a href="support/">пишите нам</a>, будем рады ответить!</div>
                    </div>
                    <div class="cls">&nbsp;</div>
                </div></div>
            <div class="cls"></div></div>
    </main>
@endsection