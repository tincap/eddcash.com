@extends('case')
@section('content')
<div class="inner">
   <div class="contest">
      <h1>{{@lang('Drawing_of_lottery_of_Mac_Book_Pro_-_is_Mega_one_!')}}</h1>
      <div class="cls"></div>
      <div class="contest-header">
         <div class="in">
            <div class="left">
               <img src="http://WebCash.top//uploads/macbookpro.png" alt="Розыгрыш MacBook Pro - Мега розыгрыш !">
            </div>
            <div class="right">
               <h3>{{@lang('To_participate_in_the_contest_it's_necessary')}}:</h3>
               <div class="rule"><span class="num">1</span> {{@lang('Fill_in_the_balance_for_300_rubles')}}
                 @if($mo >= 300 || $mo2 >= 300)
                    <span class="num">✔</span>
                @else
                    <span class="num">✖</span>
                 @endif
               </div>
               <div class="rule"><span class="num">2</span> {{@lang('Open 10 or more cases')}}
                 @if($ce >= 15)
                  <span class="num">✔</span>
                 @else
                  <span class="num">✖</span>
                 @endif
               </div>
               <div class="rule"><span class="num">3</span> {{@lang('Join_our')}} <a href="https://vk.com/vk.webcash" target="_blank" rel="nofollow">{{@lang('group_in_vkontakte')}}</a>
                 @if($mo >= 300 || $mo2 >= 300)
                  @if($ce >= 15)
                    <span class="num">✔</span>
                  @endif
                  @else
                 @endif
               </div>
               <div class="cls"></div>
               <div class="contest-countdown-title">{{@lang('the_contest_in')}}</div>
               <div class="contest-countdown" id="contest-countdown">14 <span>{{@lang('days')}}.</span> 06:18:20</div>
            </div>
            <div class="cls"></div>
         </div>
         <div class="animation"></div>
      </div>
      <div class="more-placec">
         <div class="place" style="width: 33.333333333333%">
            <div class="in">
               <div class="title">{{@lang('Smart_watches_Apple_watch')}}</div>
               <div class="img"><img src="http://unioncash.ru/uploads/contest/applewatch.png" alt="Умные часы Apple Watch"></div>
               <div class="p">{{@lang('2nd_place')}}</div>
            </div>
         </div>
         <div class="place" style="width: 33.333333333333%">
            <div class="in">
               <div class="title">{{@lang('Earphones_Air_pods')}}</div>
               <div class="img"><img src="http://unioncash.ru/uploads/contest/air-pods.png" alt="Наушники Air Pods"></div>
               <div class="p">{{@lang('3rd_place')}}</div>
            </div>
         </div>
         <div class="place" style="width: 33.333333333333%">
            <div class="in">
               <div class="title">{{@lang('Itunes_card_on_5000_rubles')}}</div>
               <div class="img"><img src="http://unioncash.ru/uploads/contest/itunes-5000.png" alt="Карта iTunes 5000р"></div>
               <div class="p">{{@lang('4th_place')}}</div>
            </div>
         </div>
         <div class="cls"></div>
      </div>
      <div class="contest-desc">
         <b>WebCash </b> {{@lang('draws_lots_for')}} <strong>
           <span style="color: #ffffff;">MacBook Pro </span>
           , <span style="color: #ffffff;">Apple Watch </span>
         </strong>
         <span style="color: #ffffff;">(Sport Edition</span>
         <span style="color: #ffffff;">)</span>
         <strong>,
           <span style="color: #ffffff;">Air Pods</span> и <span style="color: #ffffff;">iTunes Gift Card 5000р</span>
         </strong> &nbsp;{{@lang('for_the_most_active_project_members.To_participate_in_the_contest_it's_necessary_to_comply_with_the_conditions_that_are_above_on_the_contest_page._Conditions_must_be_complied_during_the_period_of_the_contest._Only_the_cases'_opening_and_balance_refilling_made_during_the_contest_are_counted._The_winner_will_be_chosen_randomly,_via_the_system_random.org_at_the_last_day_of_the_contest._PS please,_don't_forget_to_susbscribe_to_our_group_in_vkontakte._Last_time_more_than_30_participants_could_be_the_winners_but_failed_because_of_not_implementing_the_last_and_the_easiest_point._Good_luck_to_al_of_you!')}}
      </div>
      <div class="part-important text-center infobox">
        <b>{{@lang('pay_attention!')}}</b> {{@lang('data_in_the participants'_table_is_renewed_each_hour')}}
      </div>
      <div class="contest-users">
        <h3>{{@lang('In_the_contest_participate')}}</h3>
        <div class="cls"></div>
        <div class="participate">
          <div class="part part-header">
            <div class="p-n">№</div>
            <div class="p-id">ID</div>
            <div class="p-name">{{@lang('full_name')}}</div>
            <div class="p-vk"></div>
            <div class="p-games">{{@lang('opened_cases')}}</div>
            <div class="p-deposit">{{@lang('filled_in')}}</div>
            <div class="cls"></div>
          </div>
          @foreach($contestants as $co)
          <div class="part">
          <div class="p-n">{{$co->id}}</div>
          <div class="p-id">{{$co->user_id}}</div>
          <div class="p-name">
            <a href="/profile/{{$co->user_id}}">{{$co->user_name}}</a>
          </div>
          <div class="p-vk">
            <a href="https://vk.com/{{$co->login}}" rel="nofollow" target="_blank">
              <span class="flaticon-soc-vk"></span>
            </a>
          </div>
          <div class="p-games">{{$co->boxes_opened}}</div>
          <div class="p-deposit">
            <span class="flaticon-check"></span>
          </div>
          <div class="cls"></div>
          </div>
          @endforeach
        </div>
      </div>
   </div>
   <script type="text/javascript" src="http://unioncash.ru/templates/frontend/default/js/jquery.countdown.min.js"></script>
   <script type="text/javascript">
      $("#contest-countdown").countdown("2017/2/20", function(event) {
      $(this).html(
      event.strftime('%D <span>дн.</span> %H:%M:%S')
      );
      });
   </script>
</div>
@endsection
