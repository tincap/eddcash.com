ALTER TABLE users ADD email varchar(255) NULL;
ALTER TABLE users ADD password varchar(255) NULL;

CREATE UNIQUE INDEX users_email_uindex ON users (email);
